import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';
import {DropdownButton, Button, MenuItem } from 'react-bootstrap';

const style = {
  display: 'inline-block',
  padding: '0 5px 0',
  width: '200px',
  lineHeight: '22px',
  marginBottom: '160px'
};

const cities = [
  {id: 0, value: 'תל אביב,חולון'},
  {id: 1, value: 'רמת גן/גבעתיים'},
  {id: 2, value: 'רמת-השרון, הרצליה'},
  {id: 3, value: 'פתח תקוה,קרית אונו ,סביון,יהוד'},
  {id: 4, value: 'כפר סבא, הוד השרון'},
  {id: 5, value: 'ירושלים, מודיעין, שוהם'},
  {id: 6, value: 'נתניה, חדרה, בנימינה, קיסריה, זכרון יעקוב'},
  {id: 7, value: 'אזור הצפון'},
  {id: 8, value: 'אזור השפלה'},
  {id: 9, value: 'אזור הדרום'}
];

const jobTypes =  [
  {id: 0, value: 'בוקר'},
  {id: 1, value: 'אחה"צ-ערב בייביסיטר'},
  {id: 2, value: 'אופר'}
];

const workDays =  [
  {id: 0, value: '1-4'},
  {id: 1, value: '5'}
];

const hoursPerDay =  [
  {id: 0, value: '1-7'},
  {id: 1, value: '8-9'}
];

const children =  [
  {id: 0, value: 'ילד/ה אחד'},
  {id: 1, value: 'תאומים'}
];

class App extends Component {
  state = {
    cityTitle: 'אזור',
    jobTitle: 'סוג משרה',    
    workDaysTitle: 'מספר ימי עבודה בשבוע',
    hoursPerDayTitle: 'מספר שעות עבודה ביום',
    numChildrenTitle: 'מספר הילדים'
  }
  onSelectCity(city) {
    this.setState({cityTitle: city.value});
  }

  onSelectJobType(job) {
    this.setState({jobTitle: job.value});
  }

  onSelectWorkDays(wd) {
    this.setState({workDaysTitle: wd.value});
  }

  onSelectHoursPerDay(hd) {
    this.setState({hoursPerDayTitle: hd.value});
  }

  onSelectChildren(cd) {
    this.setState({numChildrenTitle: cd.value});
  }

  render() {
    const {cityTitle, jobTitle, workDaysTitle, hoursPerDayTitle, numChildrenTitle} = this.state;

    return (
      <div className="App">
        <div className="dropdown">
          <DropdownButton
              bsStyle="primary"
              title={cityTitle}
              id="city-select"
          >
            {cities.map(city => <MenuItem key={city.value} onSelect={() => this.onSelectCity(city)}>{city.value}</MenuItem>)}
          </DropdownButton>
        </div>

        <div className="dropdown">
          <DropdownButton
              bsStyle="primary"
              title={jobTitle}
              id="job-type-select"
          >
            {jobTypes.map(job => <MenuItem key={job.value} onSelect={() => this.onSelectJobType(job)}>{job.value}</MenuItem>)}
          </DropdownButton>
        </div>

        <div className="dropdown">
          <DropdownButton
              bsStyle="primary"
              title={workDaysTitle}
              id="work-days-select"
          >
            {workDays.map(wd => <MenuItem key={wd.value}  onSelect={() => this.onSelectWorkDays(wd)}>{wd.value}</MenuItem>)}
          </DropdownButton>
        </div>
        
        <div className="dropdown">
          <DropdownButton
              bsStyle="primary"
              title={hoursPerDayTitle}
              id="hours-per-day-select"
          >
            {hoursPerDay.map(hd => <MenuItem key={hd.value}  onSelect={() => this.onSelectHoursPerDay(hd)}>{hd.value}</MenuItem>)}
          </DropdownButton>
        </div>

        <div>
          <DropdownButton
              bsStyle="primary"
              title={numChildrenTitle}
              id="num-children-select"
          >
            {children.map(cd => <MenuItem key={cd.value}  onSelect={() => this.onSelectChildren(cd)}>{cd.value}</MenuItem>)}
          </DropdownButton>
        </div>

      </div>
    );
  }
}

export default App;
